package com.zihadrizkyef.amanahmurattal.ui.addevent

import android.app.AlarmManager
import android.app.DatePickerDialog
import android.app.PendingIntent
import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.os.Handler
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutCompat
import android.util.AttributeSet
import android.view.View
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.zihadrizkyef.amanahmurattal.R
import com.zihadrizkyef.amanahmurattal.data.MyRepo
import com.zihadrizkyef.amanahmurattal.data.dataclass.CalendarEvent
import com.zihadrizkyef.amanahmurattal.data.local.SharedPref
import kotlinx.android.synthetic.main.activity_add_event.view.*
import java.util.*

class AddEventCustomView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
) : LinearLayoutCompat(context, attrs, defStyleAttr) {
    companion object {
        const val KEY_EVENT_INDEX = "event-index"
    }

    private var alarmMgr: AlarmManager
    private var repo: MyRepo

    private var selectedYear = 0
    private var selectedMonth = 0
    private var selectedDate = 0
    private var selectedHour = 0
    private var selectedMinute = 0

    var onCreateEventListener: AddEventListener? = null

    init {
        inflate(context, R.layout.activity_add_event, this)

        alarmMgr = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager

        val account = GoogleSignIn.getLastSignedInAccount(context)!!
        val sharedPref = SharedPref(context, account.email!!)
        repo = MyRepo(context, sharedPref, null)

        btCreate.setOnClickListener {
            if (isFormValid()) {
                showProgressBar()
                addEventToCalendar()
            }
        }

        etDate.setOnClickListener {
            showDatePicker()
        }
    }

    private fun showDatePicker() {
        val calendar = Calendar.getInstance()
        val dateListener = DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
            selectedDate = dayOfMonth
            selectedMonth = month
            selectedYear = year

            showTimePicker()
        }

        DatePickerDialog(
                context,
                dateListener,
                calendar[Calendar.YEAR],
                calendar[Calendar.MONTH],
                calendar[Calendar.DATE]
        ).show()
    }

    private fun showTimePicker() {
        val calendar = Calendar.getInstance()
        val timeListener = TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
            selectedHour = hourOfDay
            selectedMinute = minute

            etDate.setText("$selectedDate/${selectedMonth + 1}/$selectedYear $selectedHour:$selectedMinute")
        }

        TimePickerDialog(
                context,
                timeListener,
                calendar[Calendar.HOUR_OF_DAY],
                calendar[Calendar.MINUTE],
                true
        ).show()
    }

    private fun addEventToCalendar() {
        val eventList = repo.eventList
        val newEvent = CalendarEvent(
                selectedDate,
                selectedMonth + 1,
                selectedYear,
                selectedHour,
                selectedMinute,
                etName.text.toString(),
                etDescription.text.toString()
        )

        eventList.add(newEvent)
        repo.eventList = eventList

        setAlarmManager()
        showSnackbarSuccess()
    }

    private fun clearView() {
        etDate.setText("")
        tilDate.error = ""

        etName.setText("")
        tilName.error = ""

        etDescription.setText("")

        progressBar.visibility = View.GONE
        progressBar.scaleX = 0F
        progressBar.scaleY = 0F

        btCreate.visibility = View.VISIBLE
        btCreate.scaleX = 1F
        btCreate.scaleY = 1F
    }

    private fun setAlarmManager() {
        val intent = Intent(context, EventReceiver::class.java)
        intent.putExtra(KEY_EVENT_INDEX, repo.eventList.size - 1)
        val alarmIntent = PendingIntent.getBroadcast(context, 0, intent, 0)

        val calendar: Calendar = Calendar.getInstance().apply {
            set(Calendar.YEAR, selectedYear)
            set(Calendar.MONTH, selectedMonth)
            set(Calendar.DATE, selectedDate)
            set(Calendar.HOUR_OF_DAY, selectedHour)
            set(Calendar.MINUTE, selectedMinute)

            add(Calendar.MINUTE, -30)
        }

        alarmMgr.set(
                AlarmManager.RTC_WAKEUP,
                calendar.timeInMillis,
                alarmIntent
        )
    }

    private fun showProgressBar() {
        btCreate.animate().scaleX(0F).scaleY(0F).withEndAction { btCreate.visibility = View.GONE }.start()
        progressBar.animate().scaleX(1F).scaleY(1F).withStartAction { progressBar.visibility = View.VISIBLE }.start()
    }

    private fun showSnackbarSuccess() {
        Snackbar.make(constraintLayout, "Success creating event", Snackbar.LENGTH_LONG).show()
        Handler().postDelayed({
            clearView()
            onCreateEventListener?.onCreateSuccess()
        }, 1500)
    }

    private fun isFormValid(): Boolean {
        if (etDate.text.toString().isBlank()) {
            tilDate.error = "Please choose date and time"
            return false
        }

        if (etName.text.toString().isBlank()) {
            tilName.error = "Please fill the name"
            etName.requestFocus()
            return false
        }

        if (etDate.text.toString().isBlank()) {
            tilDate.error = "Please fill the date"
            tilDate.requestFocus()
            return false
        }

        return true
    }
}