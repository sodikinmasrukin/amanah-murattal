package com.zihadrizkyef.amanahmurattal.ui.wishlist

import android.content.Context
import android.support.v7.widget.LinearLayoutCompat
import android.support.v7.widget.LinearLayoutManager
import android.util.AttributeSet
import android.view.View
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.zihadrizkyef.amanahmurattal.R
import com.zihadrizkyef.amanahmurattal.data.MyRepo
import com.zihadrizkyef.amanahmurattal.data.dataclass.findresponse.TrackX
import com.zihadrizkyef.amanahmurattal.data.local.SharedPref
import kotlinx.android.synthetic.main.activity_wishlist.view.*

class WishlistCustomView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayoutCompat(context, attrs, defStyleAttr), WishListView {
    private var presenter: WishListPresenter
    private var adapter: WishListAdapter
    private val list = arrayListOf<TrackX>()

    init {
        View.inflate(context, R.layout.activity_wishlist, this)

        val account = GoogleSignIn.getLastSignedInAccount(context)!!
        val sharedPref = SharedPref(context, account.email!!)
        val repo = MyRepo(context, sharedPref, null)
        presenter = WishListPresenter(repo, this)
        adapter = WishListAdapter(context, list, repo)

        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = adapter
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()

        presenter.getWishList()
    }

    override fun showTrack(trackList: ArrayList<TrackX>) {
        list.clear()
        list.addAll(trackList)
        adapter.notifyDataSetChanged()
    }

}