package com.zihadrizkyef.amanahmurattal.ui.find

import com.zihadrizkyef.amanahmurattal.data.MyRepo
import com.zihadrizkyef.amanahmurattal.data.dataclass.findresponse.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FindPresenter(private val repo: MyRepo, private val view: FindView) {
    fun findTrack(query: String) {
        view.showProgressBar()
        repo.findMurattal(query, object:Callback<ResponseBody>{
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                view.showFindNoNetwork()
                view.hideProgressBar()
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                view.showTrack(response.body()!!.message.body.track_list)
                view.hideProgressBar()
            }
        })
    }
}