package com.zihadrizkyef.amanahmurattal.data

import android.accounts.Account
import android.content.Context
import com.zihadrizkyef.amanahmurattal.data.dataclass.CalendarEvent
import com.zihadrizkyef.amanahmurattal.data.dataclass.findresponse.ResponseBody
import com.zihadrizkyef.amanahmurattal.data.dataclass.findresponse.TrackX
import com.zihadrizkyef.amanahmurattal.data.local.SharedPref
import com.zihadrizkyef.amanahmurattal.data.network.NetworkMethod
import retrofit2.Callback

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 25/03/19.
 */

class MyRepo(private val context: Context,
             private val sharedPref: SharedPref?,
             private val network: NetworkMethod?) {
    fun findMurattal(query: String, callback: Callback<ResponseBody>) {
        network!!.findMurattal(query).enqueue(callback)
    }

    var logedInAccount: Account?
        get() = sharedPref!!.account
        set(account) { sharedPref!!.account = account }

    var wishList: ArrayList<TrackX>
        get() = sharedPref!!.wishList
        set(wishList) { sharedPref!!.wishList = wishList }

    var eventList: ArrayList<CalendarEvent>
        get() = sharedPref!!.eventList
        set(eventList) {
            sharedPref!!.eventList = eventList
        }
}