package com.zihadrizkyef.amanahmurattal.data.dataclass.findresponse

data class Message(
    val body: Body,
    val header: Header
)